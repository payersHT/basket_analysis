** Requested By: **
Carissa Torres

** Overview:**
Would you be able to run a basket analysis on 2020 Q1 sales for web? As we move into Q1 of 2021, 
we would like to understand what additional classes we need to support in the Mini Hub.

** Time Frame:**
Q1 for 2020

