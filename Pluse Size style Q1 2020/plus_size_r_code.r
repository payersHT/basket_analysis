library(tidyverse)
library(zipcode)
library(DBI)
library(odbc)
library(arules)

setwd("C:/Users/payers/Desktop")

con <- dbConnect(odbc::odbc(),
                 driver = 'SQL Server',
                 database = 'SizingAnalysis',
                 trusted = "yes",
                 Server = 'CASQL01PC2\\APP02')

store_plan <- dbGetQuery(con,"SELECT Master_Style,
style_number,
sku_number,
style_description,
department,
class,
subclass,
license,
sub_license,
body_style_silhouette,
internet_exclusive,
plus_size 
FROM SizingAnalysis.dbo.dim_product
WHERE (plus_size = 'Y'
AND division = '1') 
OR 
( division = '1' AND
internet_exclusive = 'Y')")

sales_data <- read.csv("Basket_data_q1_2020.csv")

Plus_size_sku <- store_plan %>% filter(plus_size == 'Y') %>% select(sku_number,plus_size)

plus_size_trans_list <- sales_data %>% filter(SKU %in% Plus_size_sku$sku_number) %>% select(TRANSACTION_NUMBER) %>% distinct()

filter_sales_data <- sales_data %>% filter(TRANSACTION_NUMBER %in% plus_size_trans_list$TRANSACTION_NUMBER)

HT_plus_size_sale_data <- filter_sales_data %>%
                          filter(BRAND == 'HT') %>%
                          inner_join(store_plan, by = c('SKU'= 'sku_number')) %>% 
                          filter(plus_size == 'Y' | internet_exclusive == 'Y') 

HT_pluse_size_sales_data_update_class <- HT_plus_size_sale_data %>% replace_na(list (plus_size = "N", internet_exclusive = 'N')) %>%
                                          mutate(CLASS_ID = if_else( (plus_size == "Y" & internet_exclusive =='Y'), paste(class,"both IE and PS"),
                                                            if_else((plus_size == "Y" & internet_exclusive =='N'), paste(class,"PS",sep ="_"),
                                                            if_else((plus_size == "N" & internet_exclusive =='Y'), paste(class,"IE",sep="_"),"")))) %>%
                                          select(TRANSACTION_NUMBER,CLASS_ID)
                                        
                                        colnames(HT_pluse_size_sales_data_update_class) <-  c("transactionid","updated_class_name")

write_csv(HT_pluse_size_sales_data_update_class,"HT_plus_size_sale_data_Class_id.csv")

##############################
trans <- read.transactions("HT_plus_size_sale_data_Class_id.csv",
                           format = "single", 
                           rm.duplicates =  TRUE,
                           sep = ",", 
                           cols = c("transactionid","updated_class_name"), header = TRUE)

#making the baskit
basket <- apriori(trans,parameter = list(sup = .001, conf = .001, target = "rules"))

#making them a data frame
samplebasket <- as(basket,"data.frame")

samplebasket_1 <- separate(samplebasket,1,c('LHS','RHS'),sep = '=>') 
#writing the data as a csv
write.csv(samplebasket_1,"HT_plus_size_sale_data_class_id_basket.csv")
##############################

